# Projet rentrée deploy back

## TODO
- [ ] arret du déploiement si une erreur survient dans une des étapes (à faire quand l'étape sonar fonctionnera)

## Prérequis : Machine Virtuelle

Exemple de configuration pour avoir une VM en local :

- Télécharger et installer VirtualBox : https://www.virtualbox.org/wiki/Downloads
- Installer n'importe quelle distribution linux sur la VM, dans notre cas Debian 12 : https://www.youtube.com/watch?v=Ool7lkJ5gZ0
- Avant de lancer la VM, configurer le ssh :
  - Aller dans la configuration de la VM, puis dans réseau. Selectionner 'NAT' comme mode d'accès réseau.
  - Allez ensuite dans avancé, puis dans redirection de port. Dans ce menu, cliquer en haut a droite "ajouter une nouvelle règle de redirection", dans cette règle, sélectionner comme protocole TCP, comme port d'hôte 3022 et comme port invité 22.
- Démarrez la VM, puis installer openssh-server avec la commande suivante : 
```
sudo apt-get update
sudo apt-get install openssh-server
sudo nano /etc/ssh/sshd_config
```
- Ajouter dans le fichier config ouvert : "Port 22" et "PermitRootLogin yes"
```
sudo systemctl start sshd // pour lancer le service
sudo systemctl enable sshd // pour que le service se lance au démarrage de la VM
sudo systemctl status sshd // vérifier que le service est bien lancé
```
- Vous devriez pour vous connecter en ssh avec la commande suivante : ssh root@localhost -p 3022

- Ensuite, installer git sur la VM :
```
sudo apt-get install git
git config --global user.name "Votre Nom"
git config --global user.email "votremail@gmail.com"
```
- Installer docker sur la VM en suivant ces instructions : https://docs.docker.com/engine/install/debian/
- cloner le repo des scripts de déploiement globaux :
```
git clone https://github.com/CyrilDemand/deploy_vm.git
cd deploy_vm
chmod u+x *
cd init
chmod u+x *
cd ../
./init.sh //installe docker si nécessaire et créer les environnements green et blue
```
Vous êtes maintenant prêts à configurer le back de l'application de déploiement.

## Config du back de l'application de déploiement

.env a la racine du projet :
```
GITHUB_CLIENT_ID=32c908c4e3c0cd6d0dc7
GITHUB_CLIENT_SECRET=470217fd3de3d7d0d45dc76f8ef0e12657270628
SESSION_SECRET=iddesession0123456789
PORT=3001
CALLBACK_URL=http://localhost:3000

MONGODB_URI=mongodb+srv://noedelcroix59:projetrentreedeploy@projet-rentree-deploy.r1fadx9.mongodb.net/?retryWrites=true

NGROK_AUTH_TOKEN=2ZQyXAPg607PzmVvnwp3I2bJMd4_uTygy8R2jzZadJtrDybS
GITLAB_WEBHOOK_SECRET=b2GJOYxxDLNcXUe
GITHUB_WEBHOOK_SECRET=pO0Yd50jqsaJ6pW //peut être modifié dans le webhook de github

SOCKET_PORT=4000

//eventuellement a changer selon comment votre VM est setup
SSH_HOST=localhost
SSH_PORT=3022
SSH_USERNAME=root
SSH_PASSWORD=admin
SSH_DEPLOY_PATH=/home/user/deploy_vm //à modifier avec l'endroit où vous avec clone les scripts de déploiement globaux
```

## Lancement

```
npm install
npm run start
```
Si tous se passe bien, dans la console, au démarrage du serveur, vous devriez voir :
- Socket server listening on port 4000
- Serveur démarré sur http://localhost:3001
- SSH connection established ! root@localhost:3022
- Nom de l'hôte du serveur: debian
- Mongoose is connected to mongodb+srv://noedelcroix59:projetrentreedeploy@projet-rentree-deploy.r1fadx9.mongodb.net/?retryWrites=true
- ngrok tunnel set up: https://fa65-2a01-e0a-c65-c500-2889-66c9-6a1f-5cca.ngrok-free.app (l'url va changer à chaque lancement du serveur)

Vous pouvez maintenant lancer le front de l'application de déploiement et comment à utiliser l'application. Félicitations !