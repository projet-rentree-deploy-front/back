const { NodeSSH } = require('node-ssh');

let instance = null;

async function getSSHInstance() {
    if (instance) {
        return instance;
    }

    instance = new NodeSSH();
    try {
        await instance.connect({
            host: process.env.SSH_HOST || 'localhost',
            port: process.env.SSH_PORT || 3022,
            username: process.env.SSH_USERNAME || 'root',
            password: process.env.SSH_PASSWORD
        });
        console.log('SSH connection established ! ' + process.env.SSH_USERNAME+'@'+process.env.SSH_HOST+':'+process.env.SSH_PORT);
    } catch (error) {
        console.log('Error when connecting to the VM:', error);
        instance = null;
        throw error;
    }

    return instance;
}

async function initSSH() {
    try {
        const ssh = await getSSHInstance();
        const result = await ssh.execCommand('hostname');
        console.log('Nom de l\'hôte du serveur: ' + result.stdout);
    } catch (error) {
        console.error('Erreur:', error);
    }
}

module.exports = { getSSHInstance,initSSH };