const http = require('http');
const socketIo = require('socket.io');
const socket = require('./socket');
require('dotenv').config();


const createSocketServer = (app) =>{
    const server = http.createServer(app);

    socket.init(server);

    const port = process.env.SOCKET_PORT;

    server.listen(port, () => {
        console.log('Socket server listening on port '+port);
    });
}

module.exports = createSocketServer;

