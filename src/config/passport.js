const passport = require('passport');
const GitHubStrategy = require('passport-github2').Strategy;
require('dotenv').config();
const User = require('../models/user');

passport.use(new GitHubStrategy({
        clientID: process.env.GITHUB_CLIENT_ID,
        clientSecret: process.env.GITHUB_CLIENT_SECRET,
        callbackURL: "http://localhost:3001/auth/github/callback"
    },
    function(accessToken, refreshToken, profile, done) {
        profile.accessToken = accessToken;
        done(null, profile);
    }
));

passport.serializeUser(function(user, done) {
    const obj = {
        id: user.id,
        accessToken: user.accessToken
    }
    done(null, obj);
});

passport.deserializeUser(async function(obj, done) {
    try {
        const userDoc = await User.findById(obj.id);
        if (userDoc) {
            let user = userDoc.toObject();
            user.accessToken = obj.accessToken;
            done(null, user);
        } else {
            done(null, false);
        }
    } catch (error) {
        console.error('Error in deserializeUser:', error);
        done(error, null);
    }
});