const cors = require("cors");

const setupCORSconfig=(app)=>{
    const corsOptions = {
        origin: 'http://localhost:3000',
        optionsSuccessStatus: 200,
        credentials: true
    };

    app.use(cors(corsOptions));
}

module.exports = setupCORSconfig;



