const mongoose = require('mongoose');
require('dotenv').config();

const connectToDatabase = () => {
    const dbURI = process.env.MONGODB_URI;

    mongoose.connect(dbURI,{useNewUrlParser: true, useUnifiedTopology: true });

    mongoose.connection.on('connected', () => {
        console.log('Mongoose is connected to ' + dbURI);
    });

    mongoose.connection.on('error', (err) => {
        console.log('Mongoose connection error: ' + err);
    });

    mongoose.connection.on('disconnected', () => {
        console.log('Mongoose is disconnected');
    });

    process.on('SIGINT', async () => {
        try {
            await mongoose.connection.close();
            console.log('Mongoose connection is disconnected due to application termination');
            process.exit(0);
        } catch (err) {
            console.error('Error closing the mongoose connection: ' + err);
            process.exit(1);
        }
    });
};

module.exports = connectToDatabase;