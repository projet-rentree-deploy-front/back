const Pipeline = require('../models/pipeline');
const PipelineStep = require("../models/PipelineStepEnum");
const { NEW } = require("../models/pipelineStateEnum");
const {getIO} = require("../config/socket");
const PipelineState = require("../models/pipelineStateEnum");
const {createRepository, getRepositoryById} = require('./repositoryService');
const {clone,step} = require('./sshService');



const runningPipelines = {};

function hasPipelineRunning(repoId) {
    return !!runningPipelines[repoId];
}
function isPipelineRunning(pipelineId) {
    const runningPipelineIds = Object.values(runningPipelines).map(id => id.toString());
    return runningPipelineIds.includes(pipelineId);
}

function startPipeline(pipeline) {
    if (hasPipelineRunning(pipeline.repoId)) {
        return false;
    }
    runningPipelines[pipeline.repoId] = pipeline._id;
    const io = getIO();
    io.emit('pipelineStarted', pipeline.toJSON());
    return true;
}

function closePipeline(pipeline) {
    delete runningPipelines[pipeline.repoId];
    const io = getIO();
    io.emit('pipelineStopped', pipeline.toJSON());
}


async function runPipelineStep(pipeline,stepEnum){
    await updatePipelineStep(pipeline._id, stepEnum, PipelineState.RUNNING);
    result = await step(pipeline,stepEnum.toString());
    let updatedPipeline;
    if (!result.success) {
        updatedPipeline = await updatePipelineStep(pipeline._id, stepEnum, PipelineState.FAILED, result.message);
        closePipeline(updatedPipeline);
    }else{
        updatedPipeline = await updatePipelineStep(pipeline._id, stepEnum, PipelineState.COMPLETED, result.message, false);
    }
    return {updatedPipeline:updatedPipeline, success: result.success};
}

async function runPipeline(pipeline){
    const repoId = pipeline.repoId;
    const id = pipeline._id;

    if (hasPipelineRunning(pipeline.repoId)) {
        return null;
    }

    try {
        for (let step of pipeline.steps) {
            await updatePipelineStep(id, step.step, PipelineState.NEW,'', false);
        }

        await startPipeline(pipeline);

        await updatePipelineStep(id, PipelineStep.CLONE, PipelineState.RUNNING);
        let result = await clone(pipeline);
        if (!result.success) {
            await updatePipelineStep(id, PipelineStep.CLONE, PipelineState.FAILED, result.message);
            closePipeline(pipeline);
            return pipeline;
        }else{
            await updatePipelineStep(id, PipelineStep.CLONE, PipelineState.COMPLETED, result.message, false);
        }

        result = await runPipelineStep(pipeline,PipelineStep.BUILD);
        if (!result.success) {
            closePipeline(pipeline);
            return result.updatedPipeline;
        }

        result = await runPipelineStep(pipeline,PipelineStep.TEST);
        if (!result.success) {
            closePipeline(pipeline);
            return result.updatedPipeline;
        }

        result = await runPipelineStep(pipeline,PipelineStep.SONAR);
        if (!result.success) {
            closePipeline(pipeline);
            return result.updatedPipeline;
        }

        result = await runPipelineStep(pipeline,PipelineStep.DEPLOY);

        closePipeline(pipeline);

        return result.updatedPipeline;
    } catch (error) {
        closePipeline(pipeline);
        throw error;
    }
}



async function createPipeline(name, branch, commitId, commitMessage, authorName, repoId, repoName, repoDescription, repoUrl) {
    await createRepository(repoId, repoName, repoDescription, repoUrl);

    const steps = Object.values(PipelineStep).map(step => ({
        step: step,
        state: NEW,
        stacktrace: null
    }));

    const pipelineData = {
        name,
        branch,
        commitId,
        commitMessage,
        authorName,
        repoId,
        steps,
        createdAt: new Date()
    };

    const newPipeline = new Pipeline(pipelineData);
    await newPipeline.save();

    const io = getIO();
    const newPipelineJSON = newPipeline.toJSON();
    io.emit('pipelineCreated', newPipelineJSON);

    return newPipeline;
}


async function createPipelineFromDataOfApiCall(repoId, pipelineName, commitId, commitMessage, authorName) {
    const repository = await getRepositoryById(repoId);

    if (!repository) {
        throw new Error(`Repository with ID ${repoId} not found`);
    }

    return createPipeline(
        pipelineName,
        null,
        commitId,
        commitMessage,
        authorName,
        repoId,
        repository.name,
        repository.description,
        repository.url
    );
}

// LEGACY : Gitlab (on utilise Github maintenant)

// async function createPipelineFromGitlabData(data) {
//     const lastCommit = data.commits[data.commits.length - 1];
//     const pipelineName = `pipeline-${data.ref.split('/').pop()}-${new Date().getTime()}`;
//
//     return createPipeline(
//         pipelineName,
//         data.ref.split('/').pop(),
//         lastCommit.id,
//         lastCommit.message,
//         lastCommit.author.name,
//         data.project_id,
//         data.project.name,
//         data.project.description,
//         data.project.http_url
//     );
// }

async function createPipelineFromGithubData(payload) {
    payload = JSON.parse(payload);
    const headCommit = payload.head_commit;
    const pipelineName = `pipeline-${payload.ref.split('/').pop()}-${new Date().getTime()}`;

    return createPipeline(
        pipelineName,
        payload.ref.split('/').pop(),
        headCommit.id,
        headCommit.message,
        headCommit.author.name,
        payload.repository.id,
        payload.repository.name,
        payload.repository.description,
        payload.repository.clone_url
    );
}

async function updatePipelineStep(pipelineId, stepName, newState, stacktrace='', updateSocket=true) {
    const pipeline = await Pipeline.findById(pipelineId);

    if (!pipeline) {
        throw new Error('Pipeline not found');
    }

    const step = pipeline.steps.find(s => s.step === stepName);
    if (!step) {
        throw new Error('Step not found');
    }

    step.state = newState;
    step.stacktrace = stacktrace;
    await pipeline.save();

    const updatedPipeline = await Pipeline.findById(pipelineId);

    if (updateSocket){
        const io = getIO();
        const updatedPipelineJSON = updatedPipeline.toJSON();

        io.emit('pipelineUpdated', updatedPipelineJSON);
    }

    return updatedPipeline;
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function getAllPipelines(repoId) {
    try {
        let query = {};
        if (repoId) {
            query.repoId = repoId;
        }

        const pipelines = await Pipeline.find(query);

        return pipelines.map(pipeline => {
            const {_id, name, branch, commitId, authorName, createdAt, state, repoId } = pipeline.toObject({virtuals: true});
            return {_id, name, branch, commitId, authorName, createdAt, state, repoId};
        });
    } catch (error) {
        console.error('Error retrieving pipelines:', error);
        throw error;
    }
}

async function getPipelineById(pipelineId) {
    try {
        const pipeline = await Pipeline.findById(pipelineId);
        if (!pipeline) {
            return null;
        }

        return pipeline.toObject({ virtuals: true });
    } catch (error) {
        console.error('Error retrieving pipeline by ID:', error);
        throw error;
    }
}

async function deletePipeline(pipelineId) {
    if (isPipelineRunning(pipelineId)) {
        return { success: false, message: 'Cannot delete a running pipeline' };
    }

    const pipeline = await Pipeline.findById(pipelineId);
    if (!pipeline) {
        return { success: false, message: 'Pipeline not found' };
    }

    const deleteResult = await Pipeline.deleteOne({ _id: pipelineId });
    if (deleteResult.deletedCount === 0) {
        return { success: false, message: 'No pipeline was deleted' };
    }

    return { success: true, message: 'Pipeline successfully deleted', deletedPipelineId: pipelineId };
}

module.exports = {
    createPipelineFromGithubData,
    createPipelineFromDataOfApiCall,
    updatePipelineStep,
    runPipeline,
    getAllPipelines,
    getPipelineById,
    hasPipelineRunning,
    deletePipeline,
    closePipeline,
};