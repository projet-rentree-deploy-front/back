const Repository = require('../models/repository');


async function createRepository(repositoryId,name,description,url){
    const existingRepository = await Repository.findById(repositoryId)

    if (existingRepository) return

    const repositoryData = {
        repositoryId:repositoryId,
        name:name,
        description:description,
        url:url
    };

    const newRepository = new Repository(repositoryData);
    await newRepository.save();
    return newRepository;
}

async function getAllRepositories() {
    try {
        return await Repository.find({});
    } catch (error) {
        console.error('Error retrieving repositories:', error);
        throw error;
    }
}

async function getRepositoryById(repositoryId) {
    try {
        return await Repository.findById(repositoryId);
    } catch (error) {
        console.error('Error retrieving repository:', error);
        throw error;
    }
}



module.exports = {
    createRepository,
    getAllRepositories,
    getRepositoryById
}