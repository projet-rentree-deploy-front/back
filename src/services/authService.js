const User = require('../models/user');

function isAuthenticated(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }

    res.status(401).send('User not authenticated');
}

function checkRole(role) {
    return async function(req, res, next) {
        if (!req.isAuthenticated()) {
            return res.status(401).send('Not authenticated');
        }

        try {
            const user = await User.findOne({ githubId: req.user.id });

            if (!user) {
                return res.status(404).send('User not found');
            }

            if (user.role === role) {
                next();
            } else {
                res.status(403).send('You need to have the role: ' + role + ' to do this action but you have the role: ' + user.role);
            }
        } catch (error) {
            console.error(error);
            res.status(500).send('Internal server error');
        }
    };
}

module.exports = {isAuthenticated,checkRole};

