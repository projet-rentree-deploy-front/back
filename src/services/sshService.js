const {getSSHInstance} = require("../config/ssh");
const PipelineStep = require("../models/PipelineStepEnum");
const PipelineState = require("../models/pipelineStateEnum");
const {getRepositoryById} = require("./repositoryService");




async function executeCommand(command) {
    const ssh = await getSSHInstance();
    const finalCommand = `cd ${process.env.SSH_DEPLOY_PATH} && ${command}`;

    try {
        const result = await ssh.execCommand(finalCommand);
        if (result.code !== 0) {
            return { success: false, message: result.stderr || "Unknown error occurred" };
        }
        return { success: true, message: result.stdout };
    } catch (error) {
        return { success: false, message: 'SSH command execution error: ' + error.message };
    }
}


async function getRepositoryStatus(repository) {
    try{
        const result = await executeCommand(`./info.sh ${repository.name}`);
        if (!result.success) {
            return { success: false, message: result.message };
        }
        return { success: true, message: result.message };
    }catch (error) {
        return { success: false, message: 'Error when getting repository info: ' + error.message };
    }
}


async function clone(pipeline) {
    const repository = await getRepositoryById(pipeline.repoId);

    try {
        const commandResult = await executeCommand(`./clone.sh ${repository.url} ${pipeline.commitId}`);

        if (!commandResult.success) {
            return { success: false, message: commandResult.message };
        }
        return { success: true, message: commandResult.message };
    } catch (error) {
        return { success: false, message: 'Error when running clone command: ' + error.message };
    }
}


async function step(pipeline, step) {
    const repository = await getRepositoryById(pipeline.repoId);
    const folderName = repository.name
    try {
        const commandResult = await executeCommand(`./${step}.sh ${folderName}`);

        if (!commandResult.success) {
            return { success: false, message: commandResult.message };
        }
        return { success: true, message: commandResult.message };
    } catch (error) {
        return { success: false, message: 'Error when running '+step+' command: ' + error.message };
    }
}

module.exports = {
    clone,
    step,
    getRepositoryStatus
}