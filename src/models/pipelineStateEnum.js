const PipelineState = Object.freeze({
    NEW: 'new',
    RUNNING: 'running',
    FAILED: 'failed',
    COMPLETED: 'completed',
});

module.exports = PipelineState;