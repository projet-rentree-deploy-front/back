const PipelineStep = Object.freeze({
    CLONE: 'clone',
    BUILD: 'build',
    TEST: 'test',
    SONAR: 'sonar',
    DEPLOY: 'deploy'
});

module.exports = PipelineStep;