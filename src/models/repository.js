const mongoose = require('mongoose');

const repositorySchema = new mongoose.Schema({
    _id: {
        type: String,
        alias: 'repositoryId'
    },
    name:String,
    description:String,
    url:String
});

module.exports = mongoose.model('Repository', repositorySchema);