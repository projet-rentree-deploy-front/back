const mongoose = require('mongoose');
const PipelineState = require("../models/pipelineStateEnum");
const {all} = require("express/lib/application");

const stepSchema = new mongoose.Schema({
    step: String,
    state: String,
    stacktrace: String
});

const pipelineSchema = new mongoose.Schema({
    id: mongoose.Schema.Types.ObjectId,
    repoId:String,
    name: String,
    branch: String,
    commitId: String,
    commitMessage: String,
    authorName: String,
    steps: [stepSchema],
    createdAt: Date
});


pipelineSchema.virtual('state').get(function () {
    const allStates = this.steps.map(step => step.state);

    if (allStates.includes(PipelineState.FAILED)) {
        return PipelineState.FAILED;
    } else if (allStates.includes(PipelineState.RUNNING)) {
        return PipelineState.RUNNING;
    } else if (allStates.every(state => state === PipelineState.COMPLETED)) {
        return PipelineState.COMPLETED;
    }

    return PipelineState.NEW;
});

pipelineSchema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Pipeline', pipelineSchema);
