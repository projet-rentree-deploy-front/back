const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    _id: {
        type: String,
        alias: 'githubId',
        required: true
    },
    name: String,
    picture: String,
    role: { type: String, enum: ['DEV', 'ADMIN'], default: 'ADMIN' }
});

module.exports = mongoose.model('User', userSchema);