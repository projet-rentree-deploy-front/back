const express = require('express');
const bodyParser = require('body-parser');
const session = require('express-session');
const passport = require('passport');
require('./config/passport');
const { initSSH } = require('./config/ssh');
const cors = require('cors');
const connectToDatabase = require("./config/mongo");
const createSocketServer = require("./config/socketServer")
const setupCORSconfig = require("./config/cors");

connectToDatabase();

initSSH();

const app = express();

createSocketServer(app)
setupCORSconfig(app);

app.use(session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: true,
    cookie: {
        secure: false,
        httpOnly: true
    }
}));


app.use(bodyParser.json());
app.use('/webhook/github', express.raw({ type: 'application/json' }));


app.use(passport.initialize());
app.use(passport.session());


app.use((err, req, res, next) => {
    console.error(err.stack);
    res.status(500).send('Something broke!');
});


app.use(require('./routes'));

module.exports = app;