const express = require('express');
const Pipeline = require('../models/pipeline');
const { exec } = require('child_process');
const PipelineStep = require("../models/PipelineStepEnum");
const PipelineState = require("../models/pipelineStateEnum");
const {getAllPipelines, runPipeline, getPipelineById, createPipelineFromDataOfApiCall, deletePipeline} = require("../services/pipelineService");
const {isAuthenticated, checkRole} = require("../services/authService");

const router = express.Router();


router.get('/',isAuthenticated, async (req, res) => {
    try {
        const repoId = req.query.repoId;
        const pipelines = await getAllPipelines(repoId);
        res.status(200).json(pipelines);
    } catch (error) {
        console.error('Error retrieving pipelines:', error);
        res.status(500).send('Error retrieving pipelines');
    }
});

router.get('/:id',isAuthenticated, async (req, res) => {
    const pipelineId = req.params.id;
    try {
        const pipeline = await getPipelineById(pipelineId);
        if (!pipeline) {
            return res.status(404).send('Pipeline not found');
        }
        res.status(200).json(pipeline);
    } catch (error) {
        console.error('Error retrieving pipeline:', error);
        res.status(500).send('Error retrieving pipeline');
    }
});

router.post('/', checkRole('ADMIN'), async (req, res) => {
    try {
        const { repoId, pipelineName, commitId, commitMessage} = req.body;

        if (!repoId || !pipelineName || !commitId || !commitMessage) {
            return res.status(400).send('Missing required fields');
        }
        const authorName = req.user.name;
        if (!authorName) {
            return res.status(400).send('Missing author name');
        }

        const newPipeline = await createPipelineFromDataOfApiCall(repoId, pipelineName, commitId, commitMessage, authorName);

        return res.status(201).json(newPipeline);
    } catch (error) {
        console.error('Error creating pipeline:', error);
        return res.status(500).send('Internal Server Error');
    }
});
router.post('/start/:pipelineId',checkRole('ADMIN'), async (req, res) => {


    const pipelineId = req.params.pipelineId;

    try {
        const pipeline = await Pipeline.findById(pipelineId);

        if (!pipeline) {
            return res.status(404).send('Pipeline not found');
        }

        const finalPipeline = await runPipeline(pipeline);

        if (!finalPipeline) {
            return res.status(403).send('A pipeline is already running for this repository');
        }

        return res.status(200).json(finalPipeline);

    } catch (error) {
        console.error('Error fetching pipeline from MongoDB:', error);
        res.status(500).send('Error fetching pipeline');
    }
});

router.delete('/:id', checkRole('ADMIN'), async (req, res) => {
    const pipelineId = req.params.id;

    try {
        const result = await deletePipeline(pipelineId);
        if (!result.success) {
            return res.status(400).send(result.message);
        }

        res.status(200).send(result);
    } catch (error) {
        console.error('Error:', error.message);
        res.status(500).send('Internal Server Error');
    }
});




module.exports = router;