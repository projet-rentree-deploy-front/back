const {getAllRepositories,getRepositoryById, createRepository} = require('../services/repositoryService')

const express = require("express");
const {isAuthenticated, checkRole} = require("../services/authService");
const {hasPipelineRunning} = require("../services/pipelineService");
const axios = require('axios');
const {getRepositoryStatus} = require("../services/sshService");


const router = express.Router();
router.get('/', isAuthenticated, async (req, res) => {
    try {
        const repositories = await getAllRepositories();
        res.status(200).json(repositories);
    } catch (error) {
        console.error('Error retrieving pipelines:', error);
        res.status(500).send('Error retrieving pipelines');
    }
});

router.get('/:repositoryId',isAuthenticated, async (req, res) => {
    try {
        const repository = await getRepositoryById(req.params.repositoryId);
        if (!repository) {
            res.status(404).send('Repository not found');
        } else {
            res.status(200).json(repository);
        }
    } catch (error) {
        console.error('Error retrieving repository:', error);
        res.status(500).send('Error retrieving repository');
    }
});

router.get('/haspipelinerunning/:repositoryId', isAuthenticated, async (req, res) => {
    try {
        const repositoryId = req.params.repositoryId;
        const running = hasPipelineRunning(repositoryId);
        res.status(200).json({ isRunning: running });
    } catch (error) {
        console.error('Error checking pipeline running status:', error);
        res.status(500).send('Error checking pipeline running status');
    }
});

router.get('/status/:repositoryId',isAuthenticated, async (req, res) => {
    try{
        const repository = await getRepositoryById(req.params.repositoryId);
        const result = await getRepositoryStatus(repository);
        return res.status(200).json({ isDeployed: result.message });
    }catch (error) {
        console.error('Error checking repository deployed status:', error);
        return res.status(500).send('Error checking repository deployed status');
    }
});

router.post('/',isAuthenticated, async (req, res) => {
    try {
        const repositoryData = req.body;
        const repository = await createRepository(repositoryData.repoId, repositoryData.name, repositoryData.description, repositoryData.url);
        if (!repository) {
            res.status(409).send('Repository already exists');
        }
        res.status(200).json(repository);
    } catch (error) {
        console.error('Error creating repository:', error);
        res.status(500).send('Error creating repository');
    }
});


router.get('/:repositoryId/commits', isAuthenticated, async (req, res) => {
    if (!req.user || !req.user.accessToken) {
        return res.status(401).send('User is not authenticated or accessToken is missing');
    }

    const accessToken = req.user.accessToken;
    const repositoryId = req.params.repositoryId;
    const githubApiUrl = `https://api.github.com/repositories/${repositoryId}/commits`;

    try {
        const response = await axios.get(githubApiUrl, {
            headers: { 'Authorization': `token ${accessToken}` }
        });

        const commits = response.data.map(commit => ({
            commitId: commit.sha,
            commitMessage: commit.commit.message,
            author: commit.commit.author.name
        }));

        return res.status(200).json(commits);
    } catch (error) {
        console.error('Error fetching commits:', error);
        return res.status(500).send('Error fetching commits');
    }
});

module.exports = router;