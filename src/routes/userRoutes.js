const express = require('express');
const passport = require('passport');
require('dotenv').config();
const User = require('../models/user');
const {isAuthenticated} = require("../services/authService");
const axios = require('axios');

const router = express.Router();

router.get('/info', isAuthenticated, (req, res) => {
    if (!req.user) {
        return res.status(404).send('User not found');
    }

    const userInfo = {
        id: req.user._id,
        name: req.user.name,
        picture: req.user.picture,
        role: req.user.role
    };
    res.json(userInfo);
});

router.get('/repositories', isAuthenticated, async (req, res) => {
    if (!req.user || !req.user.accessToken) {
        return res.status(401).send('User is not authenticated or accessToken is missing');
    }

    const accessToken = req.user.accessToken;
    const perPage = 100;
    const pageNumber = 1;
    const githubApiUrl = `https://api.github.com/user/repos?per_page=${perPage}&page=${pageNumber}`;

    try {
        const response = await axios.get(githubApiUrl, {
            headers: { 'Authorization': `token ${accessToken}` }
        });

        const repositories = response.data.map(repo => ({
            id: repo.id,
            name: repo.name,
            description: repo.description,
            url: repo.html_url
        }));

        return res.status(200).json(repositories);
    } catch (error) {
        console.error('Error fetching repositories:', error);
        return res.status(500).send('Error fetching repositories');
    }
});


module.exports = router;