const express = require('express');
const passport = require('passport');
require('dotenv').config();
const User = require('../models/user');
const {isAuthenticated} = require("../services/authService");

const router = express.Router();

router.get('/github', (req, res, next) => {
    const redirect = req.query.redirect || '';
    passport.authenticate('github', {
        state: encodeURIComponent(redirect)
    })(req, res, next);
});



router.get('/github/callback',
    passport.authenticate('github', { failureRedirect: '/login' }),
    async (req, res) => {
        try {
            const githubUser = req.user;

            const user = await User.findOneAndUpdate(
                { _id: githubUser.id },
                {
                    _id: githubUser.id,
                    name: githubUser.displayName,
                    picture: githubUser._json.avatar_url
                },
                { upsert: true, new: true }
            );
            const state = decodeURIComponent(req.query.state || '');
            res.redirect(process.env.CALLBACK_URL+state);
        } catch (err) {
            console.error(err);
            res.status(500).send('An error occurred');
        }
    }
);

router.get('/verify', (req, res) => {
    if (req.isAuthenticated()) {
        res.json({ authenticated: true });
    } else {
        res.json({ authenticated: false });
    }
});
module.exports = router;