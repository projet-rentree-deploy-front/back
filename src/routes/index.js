const express = require('express');
const authRoutes = require('./authRoutes');
const webHooksRoutes = require('./webHooksRoutes');
const pipelineRoutes = require('./pipelineRoutes');
const repositoryRoutes = require('./repositoryRoutes');
const userRoutes = require('./userRoutes');

const router = express.Router();

router.use('/auth', authRoutes);

router.use('/webhook', webHooksRoutes);

router.use('/pipelines', pipelineRoutes);

router.use('/repositories', repositoryRoutes);

router.use('/user', userRoutes);


module.exports = router;