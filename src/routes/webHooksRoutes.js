const express = require('express');
const {Types} = require("mongoose");
const Pipeline = require('../models/pipeline');
const PipelineStep = require("../models/PipelineStepEnum");
const {NEW} = require("../models/pipelineStateEnum");
const {createPipelineFromGithubData,runPipeline} = require("../services/pipelineService");
const crypto = require('crypto');

const router = express.Router();

// LEGACY : GitLab Webhook (on utilise maintenant GitHub)

// router.post('/gitlab', async (req, res) => {
//     const gitlabToken = req.headers['x-gitlab-token'];
//
//     if (gitlabToken !== process.env.GITLAB_WEBHOOK_SECRET) {
//         return res.status(403).send('Invalid token');
//     }
//
//     const data = req.body;
//     // Check if the webhook event is for a push to the specific branch
//     if (data.object_kind === 'push') {
//
//         // Save new pipeline to MongoDB
//         try {
//             const pipeline = await createPipelineFromGitlabData(data);
//             const completedPipeline = runPipeline(pipeline);
//             if (!completedPipeline) {
//                 return res.status(200).send('Pipeline created but could not start because another pipeline is already running');
//             }
//             return res.status(200).send('Pipeline created and started');
//         } catch (error) {
//             console.error('Error saving pipeline information to MongoDB:', error);
//             return res.status(500).send('Error saving pipeline information');
//         }
//     } else {
//         res.status(200).send('Webhook received but not processed for this event');
//     }
// });

router.post('/github', async (req, res) => {
    const githubSignature256 = req.headers['x-hub-signature-256'];
    const secret = process.env.GITHUB_WEBHOOK_SECRET;
    const payload = JSON.stringify(req.body);

    if (!verifyGitHubSignature(githubSignature256, payload, secret)) {
        return res.status(403).send('Invalid signature');
    }

    if (req.headers['x-github-event'] === 'push') {
        try {
            const pipeline = await createPipelineFromGithubData(payload);
            const completedPipeline = runPipeline(pipeline);
            if (!completedPipeline) {
                return res.status(200).send('Pipeline created but could not start because another pipeline is already running');
            }
            return res.status(200).send('Pipeline created and started');
        } catch (error) {
            console.error('Error processing GitHub webhook:', error);
            return res.status(500).send('Error processing webhook');
        }
    } else {
        return res.status(200).send('Webhook received but not processed for this event');
    }
});

function verifyGitHubSignature(signature256, payload, secret) {
    const hmac = crypto.createHmac('sha256', secret);
    const calculatedSignature = 'sha256=' + hmac.update(payload).digest('hex');
    return calculatedSignature === signature256;
}

module.exports = router;
