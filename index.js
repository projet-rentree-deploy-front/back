const app = require('./src/app');
const ngrok = require("ngrok");
require('dotenv').config();

const port = process.env.PORT || 3001;
app.listen(port, () => {
    console.log(`Serveur démarré sur http://localhost:${port}`);
});

(async function() {
    try {
        await ngrok.authtoken(process.env.NGROK_AUTH_TOKEN);
        const url = await ngrok.connect(port);
        console.log(`ngrok tunnel set up: ${url}`);
    } catch (err) {
        console.error(`Error starting ngrok: ${err.message}`);
    }
})();